package hj.com.hjbox.interf;

/**
 * 
 * @author deyi
 *
 */
public interface BaseViewInterface {
	
	public void initView();
	
	public void initData();
	
}
