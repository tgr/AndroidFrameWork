package hj.com.hjbox.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import hj.com.hjbox.R;

/**
 * 抽屉底部菜单适配器
 * Created by Tiger on 2015/12/8.
 */
public class DrawerUserMenuAdapter extends BaseAdapter{
    private String[] menuTitles;
    private TypedArray menuImages;
    private Context mContext;
    private int resLayout;

    /**
     * 构造方法
     * @param context  上下文
     * @param menuTitles 标题数组
     * @param menuImages 资源数组
     * @param resLayout adapter要渲染的布局
     */
    public DrawerUserMenuAdapter(Context context,String[] menuTitles,TypedArray menuImages,int resLayout) {
        this.mContext=context;
        this.menuImages=menuImages;
        this.menuTitles=menuTitles;
        this.resLayout=resLayout;
    }

    @Override
    public int getCount() {
        return menuTitles.length;
    }

    @Override
    public Object getItem(int i) {
        return menuTitles[i];
    }

    @Override
    public long getItemId(int i) {
        return menuImages.getResourceId(i,R.drawable.ic_launcher);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view==null){
            view= LayoutInflater.from(mContext).inflate(resLayout,null);
        }
        TextView menuTitle=(TextView)view.findViewById(R.id.menu_title);
        ImageView menuImage=(ImageView)view.findViewById(R.id.menu_icon);
        menuTitle.setText(menuTitles[i]);
        menuImage.setImageResource(menuImages.getResourceId(i, R.drawable.ic_launcher));
        return view;
    }
}
